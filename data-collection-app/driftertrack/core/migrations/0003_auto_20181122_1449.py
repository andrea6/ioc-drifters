# Generated by Django 2.0 on 2018-11-22 14:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20180821_0939'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mu201sdatapacket',
            name='imei',
            field=models.CharField(db_index=True, max_length=64),
        ),
    ]

from django.shortcuts import render

from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from .models import *
from django.urls import reverse
from django.shortcuts import get_object_or_404
import json
from django.db.models.query import QuerySet
import datetime



def mapper(request, imei=None):

    results = MU201SDataPacket.objects.distinct("imei").all()
    imeis = [ r.imei for r in results ]
    packets = None

    final = []
    NPOSITIONS = 200
    if imei:
        packets = list(MU201SDataPacket.objects.raw("SELECT * FROM core_mu201sdatapacket WHERE imei='%s' " % (imei)))
        packets.sort(key=unfuckdate)
        
        step = int(len(packets)/NPOSITIONS)
        if step<1:
            step=1
        for c in range(0, len(packets), step):
            if packets[c].content["status"] == "A":
                final.append(packets[c])



    return render(request,"mapper.html", { "imeis": imeis,"selected":imei, "packets":final  })


def unfuckdate(el):
    date = el.content["date"]
    return date[4:6]+date[2:4]+date[0:2]+el.content["time"]

def MU201S(request):
    imei = request.GET.get("imei",None)
    content = request.GET.get("rmc", None)

    if not imei or not content:
        r = HttpResponse()
        r.response_code = 500
        r.content = "MU201S requires to send an 'imei' and an 'rmc' GET parameter, which are now missing."
        return r

    csv = content.split(",")
    # Indexes:
# 0 - $GPRMC - RMC protocol header
# 1 - UTC Time of position fix, format hhmmss.ss
# 2 - Status - Navigation receiver status (A for valid, V for maybe invalid)
# 3 - Latitude format ddmm.mmmmm (degrees, minutes, and thousandts of minutes)
# 4 - South/North (capital letter S or N)
# 5 - Longitude format dddmm.mmmmm (degrees, minutes, and thousandths of minutes)
# 6 - East / West (capital letter E or W)
# 7 - Ground speed - numeric float value (NM / h)
# 8 - Direction - numeric float value (degrees true)
# 9 - Date - format ddmmyy - Current date
# 10 - null field
# 11 - null field
# 12 - Mode indicator (A for autonomous or D for differential)
# 13 - Checksum

    jsc = { "time": csv[1],
            "date": csv[9],
            "status": csv[2],
            "lat": csv[3],
            "lat_dir": csv[4],
            "lon": csv[5],
            "lon_dir": csv[6],
            "ground_speed": csv[7],
            "heading": csv[8],
            }

    dp = MU201SDataPacket()
    dp.imei = imei
    dp.content = jsc
    dp.save()
    r = HttpResponse()
    r.response_code = 200
    r.content = "OK"
    return r

from django.db import models
from django.contrib.postgres.fields import JSONField
from model_utils.models import TimeStampedModel
from django.contrib.auth.models import User
import datetime

from django.conf import settings
from django.conf.urls.static import static
from django.urls import reverse
from django.utils.safestring import mark_safe
# Create your models here.



class DataPacket(TimeStampedModel):

    class Meta:
            verbose_name = "Drifter Packet"
            verbose_name_plural = "Drifter Packets"
            abstract = True


    content = JSONField(null=True, blank= True)





class MU201SDataPacket(DataPacket):

    class Meta:
            verbose_name = "MU201S Drifter Packet"
            verbose_name_plural = "MU201S Drifter Packets"

    imei = models.CharField(max_length=64, db_index=True)

    def __str__(self):
        return "[%s] - %s" % ( self.imei, self.content )

    def track_on_map(self):
        return mark_safe('<a href="%s" >%s</a>' % (reverse('mu201smapper', kwargs={ 'imei': self.imei } ), "Track on Map" ))
    


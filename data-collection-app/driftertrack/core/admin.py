from django.contrib import admin
import json
from .models import *
import datetime
from import_export import resources, fields
from import_export.admin import ImportExportActionModelAdmin, ImportExportModelAdmin

class MU201SResource(resources.ModelResource):

    lat = fields.Field()
    lon = fields.Field()
    hour = fields.Field()
    minute = fields.Field()
    second = fields.Field()
    year = fields.Field()
    day = fields.Field()
    month = fields.Field()
    speed = fields.Field()
    heading = fields.Field()
    status = fields.Field()

    class Meta:
        model = MU201SDataPacket 
        exclude = ('created', 'modified','content','id' )
        export_order = ('imei','lat','lon','year','month','day','hour','minute','second','speed','heading','status' )

    def dehydrate_lat(self, packet):
        return packet.content["lat"]+packet.content["lat_dir"]

    def dehydrate_lon(self, packet):
        return packet.content["lon"]+packet.content["lon_dir"]

    def dehydrate_hour(self, packet):
        return packet.content["time"][0:2]

    def dehydrate_minute(self,packet):
        return packet.content["time"][2:4]

    def dehydrate_second(self,packet):
        return packet.content["time"][4:6]

    def dehydrate_year(self, packet):
        return "20"+packet.content["date"][4:6]

    def dehydrate_month(self, packet):
        return packet.content["date"][2:4]

    def dehydrate_day(self, packet):
        return packet.content["date"][0:2]

    def dehydrate_speed(self, packet):
        return packet.content["ground_speed"]

    def dehydrate_heading(self, packet):
        return packet.content["heading"]

    def dehydrate_status(self, packet):
        return packet.content["status"]

class MU201SAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    resource_class = MU201SResource 
    model = MU201SDataPacket
    list_display = [ 'imei', 'created', 'datetime', 'lat', 'lon', 'heading', 'speed','track_on_map' ]
    list_filter = [ 'imei' ]


    def lat(self, instance):
        d = instance.content
        return d["lat"]+d["lat_dir"]

    def lon(self, instance):
        d = instance.content
        return d["lon"]+d["lon_dir"]

    def datetime(self, instance):
        d = instance.content
        dt = datetime.datetime.strptime( d["date"] + " " + d["time"][:-3], "%d%m%y %H%M%S" )
        return dt
    datetime.short_description = "GPS fix timestamp"


    def heading(self, instance):
        d = instance.content
        return d["heading"]

    def speed(self, instance):
        d = instance.content
        return d["ground_speed"]


admin.site.register(MU201SDataPacket, MU201SAdmin)

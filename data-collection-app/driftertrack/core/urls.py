from django.urls import include, path
from . import views


urlpatterns = [
    path('MU201S/', views.MU201S, name='mu201s'),
    path('mapper/<str:imei>/', views.mapper, name='mu201smapper'),
    path('mapper/', views.mapper, name='mu201smapper'),

]
